package li.dadada.uuidgen.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Uuid(
    @PrimaryKey val uuid: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "created") val created: Long = Date().time
) {
    override fun toString(): String {
        return uuid
    }

    override fun equals(other: Any?): Boolean {
        return if (other !is Uuid) {
            false
        } else {
            uuid == other.uuid
        }
    }

    override fun hashCode(): Int {
        var result = uuid.hashCode()
        result = 31 * result + created.hashCode()
        return result
    }
}


