package li.dadada.uuidgen

import android.app.Application
import androidx.room.Room
import li.dadada.uuidgen.model.AppDatabase
import li.dadada.uuidgen.model.UuidDao

class UuidGen : Application() {
    internal val uuidDao : UuidDao by lazy {
        db.uuidDao()
    }

    private val db: AppDatabase by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "uuids"
        ).build()
    }
}