package li.dadada.uuidgen.ui

import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import li.dadada.uuidgen.R
import li.dadada.uuidgen.databinding.UuidGenFragmentBinding
import li.dadada.uuidgen.viewmodel.ClipBoardUtil
import li.dadada.uuidgen.viewmodel.UuidViewModel


class UuidGenFragment : Fragment(), CoroutineScope by MainScope() {

    private val viewModel: UuidViewModel by lazy {
        ViewModelProvider(this).get(UuidViewModel::class.java)
    }

    private lateinit var clipboardManager: ClipboardManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)

        val binding: UuidGenFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.uuid_gen_fragment, container, false)
        clipboardManager =
            inflater.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        binding.buttonGen.setOnClickListener { view ->
            launch { genUuid(view) }
        }
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_uuid, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).supportActionBar?.title =
                getString(R.string.title_generate_uuid)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_to_history -> {
                findNavController().navigate(R.id.action_global_uuid_list_fragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private suspend fun genUuid(view: View) {
        val uuid = viewModel.genUuid()
        if (uuid != null) {
            ClipBoardUtil.copyUuid(uuid, view, clipboardManager)
        } else {
            Snackbar.make(view, R.string.warn_duplicate_uuid, Snackbar.LENGTH_SHORT)
        }
    }
}