package li.dadada.uuidgen.viewmodel

import android.view.View
import li.dadada.uuidgen.model.Uuid

interface OnUuidClickListener {
    fun onUuidClicked(view: View, uuid: Uuid) : Boolean
}