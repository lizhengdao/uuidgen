package li.dadada.uuidgen.viewmodel

import android.app.Application
import android.content.ClipData
import android.content.ClipboardManager
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import li.dadada.uuidgen.UuidGen
import li.dadada.uuidgen.model.Uuid
import li.dadada.uuidgen.model.UuidDao


internal class UuidViewModel(application: Application,
                             savedStateHandle: SavedStateHandle
) : AndroidViewModel(application) {

    private val uuidDao : UuidDao by lazy {
        (application as UuidGen).uuidDao
    }

    val uuidList = uuidDao.getAll()

    suspend fun genUuid() : Uuid? {
        return genUuid(uuidDao)
    }

    companion object {
        private suspend fun genUuid(uuidDao: UuidDao) : Uuid? = withContext(Dispatchers.IO) {
            val uuid = Uuid()
            with(uuidDao) {
                if (get(uuid.uuid).isEmpty()) {
                    insertAll(uuid)
                    uuid
                } else {
                    null
                }
            }
        }
    }
}